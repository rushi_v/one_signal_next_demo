import styled, { css } from "styled-components";

import * as g from "../../styles/global/main.style";
import { mediaQueries } from "../../utils/mediaQuery";

export const HeaderComponentWrapper = styled.div`
  position: relative;
  display: block;
  width: 100%;
  min-height: 60px;
  height: 100%;
  z-index: 1000;

  /* Props */
  ${(props) =>
    props.isSellerSubComponent &&
    css`
      overflow: hidden;
    `}

  ${(props) =>
    props.isSellerSubPlan &&
    css`
      margin-bottom: 0 !important;
    `}
  & > .react-bootstrap-nav {
    background-color: var(--primaryColor);
    padding: 0px;
    min-height: 60px;
    /*  */
    .container-fluid {
      display: flex;
      padding: 2px 60px;
      margin: 0;
      width: 100%;
      ${mediaQueries("lg")`
      padding:5px 30px;
    `}
      ${mediaQueries("md")`
      padding-left: 15px;
      padding-right: 15px;
    `}
    }
  }
`;

export const RightSideDiv = styled.div`
  height: 50px;
  display: flex !important;
  align-items: center;
  margin: 0 -10px;
`;

export const navbar = styled.div`
  padding: 0px;
`;

export const HeaderLogoMainDiv = styled.div`
  width: 81px;
  /* padding-top: 2px; */
  cursor: pointer;
  svg {
    height: 100%;
    width: 100%;
  }
  /* Media query */
  ${mediaQueries("lg")`
    position: absolute;
    left: 50%;
    transform: translate(-50%, 0);
    width: 75px;
  `}
`;
export const FfSignUpBtnMainDiv = styled.div`
  width: auto;
  display: flex;
  align-items: center;

  /* Media query */
  ${mediaQueries("xl")`
    width: auto;
  `}
  ${mediaQueries("lg")`
    width: auto;
  `}
`;
export const HeaderMenuAnchor = styled.a`
  font-size: 16px;
  line-height: 16px;
  font-weight: 500;
  color: var(--whiteTextColor) !important;
  text-transform: uppercase;
  padding: 22px 35px;
  cursor: pointer;
  ${mediaQueries("xxl")`
    padding:22px 28px;
    font-size: 15px;
  `}
  ${mediaQueries("xl")`
    padding:22px 11px;
    font-size: 14px;
  `}
  &:hover {
    color: var(--buttonHoverTextColor) !important;
    background-color: var(--headerMenuHoverColor);
    ${mediaQueries("lg")`
      border-bottom: 2px solid var(--headerMenuHoverColor);
      max-width: fit-content;
      color: var(--headerMenuHoverColor) !important;
      background-color: transparent;
    `}
  }

  /* Media query */
  /* ${mediaQueries("xl")`
       padding: 22px 20px;
    `}
  
  ${mediaQueries("xl")`
    padding: 23px 10px; 
    font-size: 14px;
    line-height: 14px;
  `} */
  ${mediaQueries("lg")`
    display: block;
    padding: 0; 
    font-size: 14px;
    line-height: 20px;
  `}
  /* Props */
  ${(props) =>
    props.isActiveMenu &&
    css`
      color: var(--buttonHoverTextColor) !important;
      background-color: var(--headerMenuHoverColor);
      ${mediaQueries("lg")`
        border-bottom: 2px solid var(--headerMenuHoverColor);
        max-width: fit-content;
        color: var(--headerMenuHoverColor) !important;
        background-color: transparent;
      `}
    `}
`;

export const RightSideActionButtonWrapper = styled.div`
  display: flex;
  margin-left: auto;
  width: auto;
  align-items: center;
`;
export const HeaderFilterIconDiv = styled.div`
  width: 20px;
  display: block;
  margin: 0 0 0 0;
  cursor: pointer;

  /* Media query */
  ${mediaQueries("lg")`
    width: 25px;
    margin: 0 0 0 8px;
  `}

  ${mediaQueries("sm")`
    width: 20px;
    margin: 0 0 0 5px;
  `}
`;
export const NotificationIconWrapper = styled.div`
  position: relative;
  margin: 0 20px 0 10px;
  display: none;

  /* Media query */
  ${mediaQueries("lg")`
    display: block;
  `}
`;
export const HomeNotificationAnchor = styled.a`
  width: 25px;
  display: block;
  cursor: pointer;
  svg {
    width: 22px;
    g {
      fill: var(--colorWhite);
    }
  }

  /* Media query */
  ${mediaQueries("sm")`
    width: 20px;
  `}
`;
export const NotificationCountDiv = styled.div`
  position: absolute;
  top: -10px;
  right: -10px;
  max-width: 50px;
  min-width: 24px;
  height: 24px;
  background-color: var(--logoColor2);
  color: var(--primaryColor);
  font-size: 12px;
  line-height: 1.2;
  border-radius: 50px;
  overflow: hidden;
  -o-text-overflow: ellipsis;
  text-overflow: ellipsis;
  white-space: nowrap;
  padding: 5px 8px;

  /* Media query */
  ${mediaQueries("sm")`
    min-width: 22px;
    height: 22px;
    font-size: 11px;
    padding: 4px 8px;
  `}
`;
export const SignUpAnchor = styled.span`
  color: var(--whiteTextColor) !important;
  font-size: 18px;
  font-weight: 400;
  padding: 9px 30px;
  text-transform: uppercase;
  cursor: pointer;

  /* Media query */
  ${mediaQueries("xl")`
        padding: 8px 15px; 
        font-size: 14px;
        line-height: 14px;
  `}

  ${mediaQueries("lg")`
       display: none;
  `}

  /* Props */
  ${(props) =>
    props.isSignUpBtn &&
    css`
      color: var(--buttonHoverTextColor) !important;
      background: var(--headerMenuHoverColor);
      border: 1px solid var(--whiteTextColor);
      box-shadow: 0px 4px 29px -6px rgba(28, 27, 27, 0.45);
      border-radius: 100px;
      transition: 0.5s;
      &:hover {
        box-shadow: none;
      }
    `}
`;

export const HeaderSearch = styled.div`
  /* width: auto;
  display: flex;
  align-items: center;
  position: relative;
  margin-right: -10px;
  display: block;
  margin: 0; */

  margin-right: -10px;
  width: 10px !important;
  display: block;
  margin: 0 0 0 0;
`;
export const SearchBar = styled.div`
  /* position: relative;
  top: 0;
  right: 0;
  left: auto;
  -webkit-transform: translate(-100%, 0%);
  -ms-transform: translate(-100%, 0%);
  transform: translate(-100%, 0%);
  -webkit-transition: all 1s;
  transition: all 1s;
  width: 50px;
  height: 45px;
  background: transparent;
  box-sizing: border-box;
  border-radius: 25px;
  padding: 5px;
  &:hover {
    width: 200px;
    background-color: var(--whiteTextColor);
    cursor: pointer;
    ${mediaQueries("lg")`
        width: 120px;
    `}
    input {
      display: block;
    }
    span {
      background-color: var(--headerMenuHoverColor) !important;
      color: var(--whiteTextColor);
    }
  }
  input {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 42.5px;
    line-height: 30px;
    outline: 0;
    border: 0;
    display: none;
    font-size: 1em;
    border-radius: 20px;
    padding: 0 20px;
  } */
  position: relative;
  // top: 50%;
  // left: 50%;
  top: 0;
  right: 0;
  left: auto;

  transform: translate(-100%, 0%);
  transition: all 1s;
  width: 50px;
  height: 45px;
  background: transparent;
  box-sizing: border-box;
  border-radius: 25px;
  padding: 5px;
  input {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 42.5px;
    line-height: 30px;
    outline: 0;
    border: 0;
    display: none;
    font-size: 1em;
    border-radius: 20px;
    padding: 0 20px;
  }
  span {
    position: absolute;
    top: 2px;
    left: auto;
    right: 1px;
    background-color: transparent;
    color: var(--whiteTextColor);
    height: 40px;
    width: 40px;
    border-radius: 50%;
    line-height: 40px;
    text-align: center;
    font-size: 16px;
    transition: all 1s;
  }

  &:hover {
    width: 200px;
    background-color: var(--whiteTextColor);
    cursor: pointer;
    input {
      display: block;
    }
    span {
      background: var(--secondaryColor);
      color: white;
    }
  }
  ${mediaQueries("lg")`
    &:active {
      width: 200px;
      background-color: var(--whiteTextColor);
      cursor: pointer;
      input {
        display: block;
      }
      span {
        background: var(--secondaryColor);
        color: white;
      }
    }
  `}
`;

export const HeaderSearchBarIconWrapper = styled.div`
  ${g.Span} {
    position: absolute;
    top: 2px;
    left: auto;
    right: 1px;
    background-color: transparent;
    color: var(--whiteTextColor);
    height: 40px;
    width: 40px;
    border-radius: 50%;
    line-height: 40px;
    text-align: center;
    font-size: 16px;
    transition: all 1s;
    ${g.Icon} {
      text-align: center;
      padding: 0;
      position: relative;
      top: 3px;
      font-weight: 900;
      color: white;
      font-size: 20px;
      cursor: pointer;

      ${mediaQueries("lg")`
      font-size: 15px;
      padding: 0 10px;
    `}
    }
  }
`;

export const headerUserIconDivMain = styled.div`
  display: flex;
  align-items: center;
  color: var(--whiteTextColor);
  margin: 0 0 0 10px;
  cursor: pointer;
`;

export const headerProfileImgDiv = styled.div`
  width: 35px;
  height: 35px;
  border-radius: 30px;
  overflow: hidden;
  display: flex;

  img {
    border-radius: 30px;
    object-fit: cover;
    width: 100%;
    height: 100%;
  }
  /* media query */
  ${mediaQueries("lg")`
    order: 1;
  `}

  ${mediaQueries("sm")`
    margin-right: 0px;
    height: 25px;
    width: 25px;
  `}
`;
export const ModalCloseIconDiv = styled.div`
  height: 24px;
  width: 24px;
  cursor: pointer;
  i {
    font-size: 14px;
    color: var(--colorWhite);
  }
  svg {
    fill: var(--colorWhite);
  }
`;

export const MultistepMain = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
`;
export const Line = styled.div`
  height: 10px;
  width: 80%;
  background: var(--inputFieldBGColor);
  border-radius: 5.5px;
  position: absolute;
  margin: 8px 50px;
`;
export const StepForm = styled.ul`
  display: flex;
  width: 100%;
  list-style: none;
  padding: 0px;
  margin: initial;
  justify-content: space-between;
  z-index: 1;

  li {
    list-style-type: none;
    position: relative;
    width: 33.33333%;
    font-weight: 400;
    border-radius: 50%;
    float: left;
    padding: 16px;
    text-align: center;
    &:after {
      content: "";
      height: 10px;
      background: var(--inputFieldBGColor);
      border-radius: 5.5px;
      top: 38px;
      left: 50%;
      width: 100%;
      display: block;
      position: absolute;
      z-index: 1;
    }
    .circle {
      cursor: pointer;
      width: 50px;
      height: 50px;
      line-height: 45px;
      display: block;
      font-size: 20px;
      color: #ffffff;
      background: var(--inputFieldBGColor);
      border-radius: 50%;
      padding: 2px;
      display: flex;
      justify-content: center;
      align-items: center;
      margin: auto;
      z-index: 111;
      text-align: center;
      margin: 0 auto 15px;
      position: relative;
      a {
        display: block;
        font-size: 14px;
        line-height: 12px;
        font-weight: 600;
        color: var(--primaryColor);
        text-align: center;
        svg {
          width: 28px;
          path {
            stroke: #fff;
          }
        }
      }
    }
    &.active {
      .circle {
        background: var(--primaryColor);
      }
      &.completed {
        &:after {
          background: var(--primaryColor);
        }
        .circle {
          background: var(--primaryColor);
        }
      }
    }
    &:last-child {
      &:after {
        content: none;
      }
    }
    span {
      font-size: 14px;
      line-height: 12px;
      font-weight: 600;
      text-align: center;
      color: var(--textDarkGreyColor);
      &.active {
        color: var(--primaryColor);
      }
    }
  }
`;
export const MultistepMainBlock = styled.div`
  display: flex;
  width: 100%;
  margin: 20px 0;
  flex-direction: column;
  justify-content: center;
  padding: 15px 0;
  border-radius: 12px;
  box-shadow: 12px 4px 80px -20px var(--BoxShadowColor);
  background-color: var(--backgroundColor);
  border: 2px solid var(--primaryColor);
  position: relative;
`;
export const MultstepContent = styled.div`
  font-weight: 500;
  font-size: 16px;
  color: var(--textDarkGreyColor);
  text-align: center;
  margin-top: 10px;
`;
export const MultistepBlock = styled.div`
  position: absolute;
  text-align: center;
  left: 0;
  right: 0;
  z-index: 111;
`;
export const CloseIcon = styled.div`
  cursor: pointer;
  position: absolute;
  right: 0;
  top: 0;
  right: 0;

  i {
    svg {
      cursor: pointer;

      path {
        cursor: pointer;
      }
    }
  }
`;
