import _ from "lodash";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { Container, Navbar } from "react-bootstrap";
import InlineSVG from "svg-inline-react";

import { ffLogoWhiteSvg } from "../../assets/svgs/allSvgOfPage";
import * as g from "../../styles/global/main.style";
import * as s from "./index.style";

const HeaderComponent = (props) => {
  const sellerHeaderOptionMobile = [
    {
      name: "Premium",
      id: 9,
      isDisplay: true,
    },
    {
      name: "Feetured Models",
      id: 15,
      isDisplay: true,
    },
    {
      name: "Sellers",
      id: 4,
      isDisplay: true,
    },
    {
      name: "Categories",
      id: 14,
      isDisplay: true,
    },
    {
      name: "How it Works",
      id: 1,
      isDisplay: true,
    },
    {
      name: "FAQ",
      id: 2,
      isDisplay: true,
    },
    {
      name: "Blogs",
      id: 10,
      isDisplay: true,
    },
    {
      name: "Contact",
      id: 3,
      isDisplay: true,
    },
  ];
  // Render Methods
  return (
    <s.HeaderComponentWrapper>
      <Navbar expand="lg" fixed="top" className="react-bootstrap-nav">
        <Container fluid>
          <s.HeaderLogoMainDiv onClick={() => onClickLogo()}>
            <InlineSVG src={ffLogoWhiteSvg} className="ff_header_logo" />
          </s.HeaderLogoMainDiv>
          <g.CollapseMenuDiv>
            <Navbar.Collapse id="basic-navbar-nav">
              <g.HeaderMenuUl>
                {sellerHeaderOptionMobile.map((option, index) => {
                  return (
                    option.isDisplay && (
                      <g.HeaderMenuLi key={index}>
                        <s.HeaderMenuAnchor role="button">
                          {option.name}
                        </s.HeaderMenuAnchor>
                      </g.HeaderMenuLi>
                    )
                  );
                })}
              </g.HeaderMenuUl>
            </Navbar.Collapse>
          </g.CollapseMenuDiv>
        </Container>
      </Navbar>
    </s.HeaderComponentWrapper>
  );
};
export default HeaderComponent;
