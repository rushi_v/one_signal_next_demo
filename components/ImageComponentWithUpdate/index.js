import React, { useEffect, useState } from "react";

const ImageComponentWithUpdate = (props) => {
  const [url, setUrl] = useState(
    props.url ||
      "https://productionspaces.nyc3.cdn.digitaloceanspaces.com/staticImages/ExtraImages/ff_Logo_white.png"
  );
  const [isError, setIsError] = useState(props.url ? false : true);

  useEffect(() => {
    if (props?.url?.trim().length > 0) {
      checkImageUrl(props.url);
    } else {
      setIsError(false);
      onError(props.url);
    }
  }, []);

  useEffect(() => {
    if (props?.url?.trim().length > 0) {
      if (props.url !== url) {
        checkImageUrl(props.url);
        setIsError(false);
      }
    }
  }, [props]);

  const checkImageUrl = async (urlData) => {
    if (urlData && urlData?.length > 0) {
      let image = new Image();
      setUrl(urlData);
      image.onload = function () {
        image = null;
      };
      image.onerror = function () {
        // image.onerror = function (e, source, no, col, error) {
        onError(urlData);
        image = null;
      };
      image.src = urlData;
    } else {
      onError(urlData);
    }
  };

  const onError = (imageUrl) => {
    if (props.isOriginalCheck) {
      if (imageUrl.includes("/thumb/")) {
        const newURL = imageUrl.replace("/thumb/", "/original/");
        setUrl(newURL);
      }
    }
  };

  return (
    <img
      alt=""
      src={url}
      className={`img-fluid  ${isError ? "ff_no_profile_img" : ""} `}
      onContextMenu={(e) => e.preventDefault()}
      onMouseDown={(e) => e.preventDefault()}
    />
  );
};

export default ImageComponentWithUpdate;
