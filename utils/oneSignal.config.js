export const getOneSignalInstance = () => {
  console.log("initializing one signal instance");
  let oneSignalInstance = window.OneSignal || [];
  return oneSignalInstance;
};

export const setUpOneSignal = (oneSignalInstance) => {
  console.log("setting up one Signal");
  oneSignalInstance.push(function () {
    oneSignalInstance.init({
      appId: "6adf3d87-508a-49b7-9f52-b8bc85c59653",
      safari_web_id: "web.onesignal.auto.2b3f64b2-7083-4ec5-9c09-3f8119751fed",
      allowLocalhostAsSecureOrigin: true,
      // notifyButton: {
      //   enable: true,
      // },
      promptOptions: {
        slidedown: {
          enabled: true,
          actionMessage:
            "We'd like to show you notifications for the latest messages and updates.",
          acceptButtonText: "Okay",
          cancelButtonText: "Cancel",
        },
      },
      // welcomeNotification: {
      //   title: "Feet Finder",
      //   message: "Thanks for subscribing!",
      // },
    });
  });
};

export const checkPushPermissionOrAsk = async (
  oneSignalInstance,
  isSupportCb,
  isEnableCb
) => {
  console.log("checking one signal push notification permission");
  // check for push notification support and ask for permission
  oneSignalInstance.push(async function () {
    // check if push notifications are supported
    const isPushNotificationsSupported =
      await oneSignalInstance.isPushNotificationsSupported();
    console.log(
      "isPushNotificationsSupported => ",
      isPushNotificationsSupported
    );
    isSupportCb(isPushNotificationsSupported);

    // continue only if push notifications are supported
    if (isPushNotificationsSupported) {
      // check if push notifications are enabled or not
      // if not enabled then show the prompt
      const isPushNotificationsEnabled =
        await oneSignalInstance.isPushNotificationsEnabled();
      isEnableCb(isPushNotificationsEnabled);
      console.log("isPushNotificationsEnabled => ", isPushNotificationsEnabled);
      if (!isPushNotificationsEnabled) {
        oneSignalInstance.showSlidedownPrompt();
      }
      return;
    }
    return;
  });
};

export const getOneSignalPersonId = async (oneSignalInstance) => {
  console.log("gettin one signal person id");
  const personId = await oneSignalInstance?.getUserId();
  return personId;
};

export const eventListnerPopoverAllowClick = (oneSignalInstance, callback) => {
  // listen for slide prompt action
  oneSignalInstance.push(function () {
    oneSignalInstance.on("popoverAllowClick", async function () {
      console.log("click allowed");
      const personId = await getOneSignalPersonId(oneSignalInstance);
      callback(personId);
    });
  });
};
export const eventListnerSubscriptionChange = (oneSignalInstance, callback) => {
  // listen for subscription change events
  oneSignalInstance.push(async function () {
    oneSignalInstance.on("subscriptionChange", async function (isSubscribed) {
      console.log("The user's subscription state is now:", isSubscribed);
      if (isSubscribed) {
        const personId = await getOneSignalPersonId(oneSignalInstance);
        localStorage.setItem("oneSignalPersonId", personId);
        callback(personId);
      }
    });
  });
};
