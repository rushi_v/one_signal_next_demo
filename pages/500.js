import Router from "next/router";
import React from "react";

const Error = () => (
  <div className="ff_error_wrapper text-center">
    <div className="logo_err">
      <img
        src={
          "https://productionspaces.nyc3.cdn.digitaloceanspaces.com/staticImages/ExtraImages/ff_Logo_white.png"
        }
        alt="ff_logo"
        className="img-fluid lazyload"
      />
    </div>
    <h1>404</h1>
    <h2>UH OH! You are lost.</h2>
    <p>
      The page you are looking for does not exist. How you got here is a
      mystery. But you can click the button below to go back to the homepage.
    </p>
    <button
      onClick={() => Router.push("/")}
      className="ff_btn_purple"
      type="button"
    >
      Home
    </button>
  </div>
);
export default Error;
