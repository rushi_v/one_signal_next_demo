import Head from "next/head";
import HeaderComponent from "../components/HeaderComponent";

export default function Home() {
  return (
    <div>
      <Head>
        <title>Demo</title>
        <meta name="description" content="demo" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <HeaderComponent>
        <main>
          <h1>
            Welcome to <a href="https://onesignal.com/">One Signal Demo</a>
          </h1>
        </main>
      </HeaderComponent>
    </div>
  );
}
