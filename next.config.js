const path = require("path");
const withSass = require("@zeit/next-sass");
const withImages = require("next-images");

const withPWA = require("next-pwa");
const runtimeCaching = require("next-pwa/cache");
const { createSecureHeaders } = require("next-secure-headers");
const isDisablePWA = process.env.PROJECT_ENV !== "production";

module.exports = withSass({
  /* bydefault config  option Read For More Optios
  here https://github.com/vercel/next-plugins/tree/master/packages/next-sass*/
  cssModules: true,
});
module.exports = {
  /* Add Your Scss File Folder Path Here */
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
};
module.exports = {
  reactStrictMode: true,
  async headers() {
    return [
      {
        source: "/(.*)",
        headers: createSecureHeaders({
          frameGuard: "sameorigin",
          xssProtection: "block-rendering",
          forceHTTPSRedirect: true,
        }),
      },
      {
        source: "/:all*(svg|jpg|png)",
        headers: [
          {
            key: "Cache-Control",
            value: "public, max-age=9999999999, must-revalidate",
          },
        ],
      },
    ];
  },
  async rewrites() {
    return {
      beforeFiles: [
        {
          source: "/(.*)",
          destination: "/500",
          has: [{ type: "header", key: "x-vercel-ip-country", value: "NG" }],
        },
      ],
      afterFiles: [],
      fallback: [],
    };
  },
  env: {
    PROJECT_ENV: process.env.PROJECT_ENV,
  },
  serverRuntimeConfig: {
    // Will only be available on the server side
    PROJECT_ENV: process.env.PROJECT_ENV,
    rollBarServerToken: process.env.ROLL_BAR_SERVER_TOKEN,
  },
  publicRuntimeConfig: {
    // Will be available on both server and client
    PROJECT_ENV: process.env.PROJECT_ENV,
    rollBarClientToken: process.env.ROLL_BAR_CLIENT_TOKEN,
  },
  images: {
    domains: [
      "productionspaces.nyc3.cdn.digitaloceanspaces.com",
      "developmentspaces.nyc3.cdn.digitaloceanspaces.com",
      "developmentspaces.nyc3.digitaloceanspaces.com",
      "productionspaces.nyc3.digitaloceanspaces.com",
      "media.feetfinder.com",
      "localhost",
      "shield.sitelock.com",
    ],
  },
  webpack: (
    config,
    { webpack } // buildId, dev, isServer, defaultLoaders,
  ) => {
    new webpack.DefinePlugin({ SC_DISABLE_SPEEDY: true });
    // Important: return the modified config
    return config;
  },
  pwa: {
    dest: "public",
    disable: isDisablePWA,
    register: true,
    skipWaiting: true,
    runtimeCaching,
  },
};
