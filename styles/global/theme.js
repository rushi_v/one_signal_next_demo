export default {
  color: {
    primary: "#7945E5",
    secondary: "#FF01C4",
    green: "#2B671D",
    white: "#ffffff",
    greyText: "#626164",
    lightGrey: "#D8D8D8",
    lightGrey2: "#B5B5B5",
    lightGrey3: "#C7C7C7",
    goldenColor: "#D5AD00",
    redColor: "#FF0000",
    greenColor: "#51A700",
    greenColor2: "#287F00",
    bG: "#F9F9F9",
    themeGradient:
      "linear-gradient(90deg, rgba(65,25,155,1) 0%, rgba(255,1,196,1) 100%)",
  },
}
