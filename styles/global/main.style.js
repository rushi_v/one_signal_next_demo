import styled, { css } from "styled-components";

import { mediaQueries } from "../../utils/mediaQuery";
import theme from "./../global/theme";

export const Icon = styled.i``;
export const Div = styled.div`
  .ff_not_found {
    text-align: center;
  }
`;
export const NavBar = styled.nav`
  background-color: var(--primaryColor);
  padding: 0px;
  min-height: 60px;
  /*  */
  .container-fluid {
    padding: 2px 60px;
    margin: 0;
    width: 100%;
    ${mediaQueries("lg")`
      padding:5px 30px;
    `}
    ${mediaQueries("md")`
      padding-left: 15px;
      padding-right: 15px;
    `}
  }
`;
export const NavBarTogglerDiv = styled.div`
  /* margin: 7px 0 0 0; */
  button {
    border: none;
    &:focus {
      outline: none;
    }
    &:hover {
      outline: none;
    }
    &:active {
      outline: none;
    }
  }
`;
export const NavBarToggleButton = styled.button`
  border: none;
  padding: 0;
  outline: none;
  color: var(--whiteTextColor);
  &:focus {
    outline: none;
  }
`;
export const HeaderMenuUl = styled.ul`
  display: flex;
  /* padding: 10px 0 0;
  margin: 0; */
  margin: 0px 0px 0px 80px;
  padding: 10px 16px;
  /* Media query */
  ${mediaQueries("lg")`
    display: block;
    margin: 0;
    padding: 18px 30px;
  `}
  ${mediaQueries("md")`
    padding: 20px 30px;
  `}
  ${mediaQueries("xl")`
      margin: 0px 0px 0px 20px;
  `}
  li {
    list-style: none;
    padding: 10px 0px;
    span {
      font-weight: 500;
      color: var(--whiteTextColor);
    }
  }
`;

export const HeaderMenuLi = styled.li`
  display: inline-block;
  padding: 10px;
  margin: 0;
  align-items: center;
  /* Media query */
  ${mediaQueries("lg")`
    display: block;
  `}
`;
export const CollapseMenuDiv = styled.div`
  /* Media query */
  ${mediaQueries("lg")`
    position: absolute;
    width: 100%; 
    left: 0px;
    background: var(--primaryColor);
    top: 60px;
  `}
`;

// Header title
export const HeaderTitleWrapper = styled.div`
  display: none;
  position: absolute;
  bottom: -24px;
  left: 0;
  right: 0;
  text-align: center;
  background-color: var(--primaryColor);
  color: var(--whiteTextColor);
  padding: 0 24px;
  margin: 0 auto;

  /* media query */
  ${mediaQueries("lg")`
      display: block;
      // bottom: -30px;
  `}
  ${mediaQueries("md")`
    padding: 0;
  `}
`;
export const BackIconWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 10px 24px;
  align-items: center;

  /* media query */
  ${mediaQueries("lg")`
        padding-top: 0;
  `}

  ${Icon} {
    display: none;
    /* media query */
    ${mediaQueries("lg")`
        display: block;
  `}
  }

  h1 {
    flex: 1;
    margin: 0 auto;
    font-size: 20px;
    line-height: 1.2;
    /* media query */
    ${mediaQueries("lg")`
        font-size: 18px;
        margin:0 !important;
  `}
  }
`;
export const Image = styled.img``;
export const Para = styled.p``;
export const Label = styled.label``;
export const Input = styled.input``;
export const Button2 = styled.button``;
export const Button = styled.button`
  background-color: var(--primaryColor);
  color: ${theme.color.white};
  font-size: 18px;
  border: none;
  border-radius: 24px;
  height: 48px;
  display: flex;
  align-items: center;
  margin: 0;
  justify-content: center;
  display: flex;
  // padding: 13px 35px 12px 25px;
  padding: 12px 20px;
  outline: 0;
  &:hover {
    background: #8c8080;
  }
  &:focus {
    outline: 0;
  }

  &.lockPackage {
    padding: 12px 25px !important;
    max-width: 170px !important;
    ${mediaQueries("sm")`
      margin:0 0 !important;
   `};
    ${mediaQueries("xss")`
        padding: 12px 9px !important;
    `}
  }
  &.unlockPackage {
    padding: 12px 40px !important;
    max-width: 170px !important;
    ${mediaQueries("sm")`
      margin:0 0 !important;
   `};
    ${mediaQueries("xss")`
        padding: 12px 25px !important;
    `}
  }
  ${mediaQueries("lg")`
    font-size: 14px;
    height: auto;
    padding: 10px 21px 9px 24px;
`};
  ${(props) =>
    props.UnLockPackage &&
    css`
      padding: 12px 40px;
      max-width: 170px !important;
    `}

  ${(props) =>
    props.LockPackage &&
    css`
      padding: 12px 26px !important;
      max-width: 170px !important;
      svg {
        display: none;
        margin-left: 9px;
      }
    `}
  ${(props) =>
    props.ReportUserbtn &&
    css`
      @media (max-width: 1399px) {
        margin-top: 20px;
      }
    `}
  ${(props) =>
    props.isTips &&
    css`
      background-color: ${theme.color.green};
      &:hover {
        background: #8c8080;
      }
    `}
  ${(props) =>
    props.isDownloadBtnAlbum &&
    css`
      background: linear-gradient(
        87.84deg,
        var(--first-color),
        var(--last-color)
      );
      &:hover {
        background: #8c8080;
      }
    `}
  ${(props) =>
    props.isReviewBtnAlbum &&
    css`
      background: linear-gradient(
        87.84deg,
        var(--first-color),
        var(--last-color)
      );
      &:hover {
        background: #8c8080;
      }
    `}
  ${(props) =>
    props.isDisable &&
    css`
      background-color: var(--text-grey3);
      svg {
        display: block;
        height: 20px;
      }
    `}
    ${(props) =>
    props.pinkBtn &&
    css`
      background-color: var(--primaryColor);
      padding: 13px auto 12px auto;
      height: auto;
      line-height: 23px;
      justify-content: center;
    `}
    ${(props) =>
    props.DownloadBtn &&
    css`
      display: block;
      width: 100%;
    `}
    ${(props) =>
    props.SendBtn &&
    css`
      width: 180px;
      height: 48px;
    `}
    ${(props) =>
    props.BtnReject &&
    css`
      background-color: ${theme.color.redColor};
    `}
    ${(props) =>
    props.IsChange &&
    css`
      width: 168px;
      padding: 9px 0;
      font-size: 20px;
      line-height: 28px;
      height: 46px;
    `}
    ${(props) =>
    props.IsDismiss &&
    css`
      background-color: transparent;
      height: auto;
      color: #777777;
      font-size: 20px;
      padding: 0;
      outline: 0;
      &:hover {
        background-color: transparent !important;
      }
    `}
    ${(props) =>
    props.checkIsLogin &&
    css`
      background-color: var(--primaryColor);
    `}
`;
export const UplaodDiv = styled.div`
  display: flex;
  align-items: center;
  margin: 0 -15px;
  flex-wrap: wrap;
`;
export const Span = styled.span`
  ${(props) =>
    props.isActive &&
    css`
      color: var(--primaryColor) !important;
      border-bottom: 1px solid var(--secondaryColor) !important;
    `}
`;
export const Card = styled.div`
  background-color: var(--lightPrimary);
  max-width: 1100px;
  margin: 0 auto;
  box-shadow: 0 0 50px 0 rgb(0 0 0 / 10%);
  margin-top: 118px;
  padding: 0 30px;
  padding-bottom: 20px;

  ${mediaQueries("lg")`
  margin-top: 68px;
  padding:0; 
  min-height:100vh;
  `};
`;

export const Tab = styled.div`
  position: relative;
  justify-content: space-between;
  display: flex;
  padding: 15px 0;
  margin-bottom: 15px;
  &:after {
    position: absolute;
    content: "";
    right: 0;
    left: 0;
    bottom: 0;
    height: 4px;
    background-image: linear-gradient(
      to right,
      var(--tabButtonGradient1),
      var(--tabButtonGradient2)
    );
    ${mediaQueries("lg")`
        height:2px;
    `};
  }
  ${mediaQueries("lg")`
    padding:15px;
  `};
  ${mediaQueries("md")`
    margin-top: 102px;
  `};
`;
export const TabMenu = styled.ul`
  padding: 0;
  list-style: none;
  margin-bottom: 0;
  margin: 0 -19px;
  ${mediaQueries("lg")`
  margin: 0 -7.5px;
`};
`;
export const TabLI = styled.li`
  display: inline-block;
  font-size: 30px;
  margin: 0 19px;
  ${mediaQueries("lg")`
  font-size: 22px;
  margin:0 7.5px;
  `};

  ${Span} {
    // color: var(--primaryColor);
    &:hover {
      color: var(--primaryColor);
      border-bottom: 1px solid var(--secondaryColor);
    }
  }
`;

export const Anchor = styled.a`
  color: var(--primaryColor);
  text-decoration: underline !important;
  cursor: pointer !important;
  display: block;
  &:hover {
    text-decoration: underline !important;
    cursor: pointer !important;
  }

  ${(props) =>
    props.isActive &&
    css`
      color: var(--primaryColor);
      border-bottom: 1px solid var(--secondaryColor);
    `}
`;

export const TabLogo = styled.div`
  svg {
    g {
      g {
        fill: var(--primaryColor);
      }
    }
    ${mediaQueries("lg")`
    
    height:28px;
  `};
  }
`;
export const SendOfferForm = styled.div`
  position: relative;
  padding-top: 50px;
  ${mediaQueries("lg")`
  padding-top: 0px;
  `};
  .ff_content_wrapper {
    margin-top: 20px;
  }
`;
export const DesktopSendOfferForm = styled.div`
  position: relative;

  .ff_content_wrapper {
    margin-top: 30px;

    .msform {
      .ff_form_control_block {
        input {
          width: 100%;
        }
        .ff_input_block {
          input {
            text-align: left;
          }
        }

        .ff_custom_select {
          margin: 30px 0 !important;
        }
      }
    }

    .ff_form_control_block {
      .ff_btn_purple {
        background-color: var(--primaryColor);
        box-shadow: none;
      }
    }
  }
`;

// new Seller signUp flow styles (welcome screen ,why do I pay screen)

export const BoxContainer = styled.div`
  width: 100%;
  background-color: var(--backgroundColor);
  margin: 0 auto;
  margin-bottom: 30px;
  border: 1px solid #cdcdcd;
  box-sizing: border-box;
  border-radius: 20px;
  padding: 40px;
  ${mediaQueries("lg")`
    margin-top: 0px;
    border: none;
    // margin-top: 67px;
  `};

  ${mediaQueries("md")`
    border-radius:0;
    min-height: auto;
    background-color: transparent;
    box-shadow: none;
     padding: 0;
  `};
  ${mediaQueries("sm")`
    margin-bottom:30px;
  `};
`;
export const WhyDoIpay = styled.div`
  margin-top: 50px;
`;
export const WelComeMain = styled.div`
  margin-top: 0px;
`;
